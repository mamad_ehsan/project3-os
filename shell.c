#include  <stdio.h>
#include  <sys/types.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <sys/wait.h>

void  parse(char *line, char **argv)
{
    char* out_address=NULL;
    char* in_address=NULL;

	while (*line != '\0') {  
		while (*line == ' ' || *line == '\t' || *line == '\n')
			*line++ = '\0'; 
		*argv++ = line; 

          


		while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') {
            line++;
            // if (*line=="<")
            //     in_address = *line;
            // printf("in *argv: %s\n" , *(argv));
            // printf("in **argv: %s\n" , (argv));            

            if (*line ==">"){
                in_address = *line;

            }
        }
        // printf("%s \n" ,(line-1));
        // printf("%s \n",line);
        // printf("\n");

	}

	*argv = '\0';

}

void  execute(char **argv)
{
     pid_t  pid;
     int    status;
     
     if ((pid = fork()) < 0) {     
          printf("*** ERROR: forking child process failed\n");
          exit(1);
     }
     else if (pid == 0) {          
          if (execvp(*argv, argv) < 0) { 
               printf("*** ERROR: exec failed\n");
               exit(1);
          }
     }
     else {                           
          while (wait(&status) != pid);
     }
}

int  main(int argc, char* argv[])
{
     char  line[1024];           
     //char  *argv[64];  
    char * token;
	char * tks [1024];
	int i = 0;
    char* out_address=NULL;
    char hostname[100];
    char *username;
    gethostname(hostname, 1024);
    username = getenv("USER");
	
     while (1) {                 
          int i =1;
          
            char * token;
            char * tks [1024];
          printf("%s@%s:$ ", username, hostname);
          gets(line);
        //   getline(&line , &linesize  , stdin);

        //   printf("%s" , line);
          	token = strtok(line, " \n");
            while (token != NULL){
                tks[i] = token;
                
                token = strtok(NULL, " \n");
                i++;
            }
            tks[i] = NULL;
            if (strcmp(tks[0], "echo") != 0){
                pid_t child_pid;
                child_pid = fork();
                if (child_pid == 0){
                    execvp(tks[0], tks);
                }
                }
                // else {
		        //     echo_handler(tks);
	            //     }


        if(out_address){
            FILE* out_file=fopen(out_address,"w+");
            dup2(fileno(out_file),STDOUT_FILENO);
            fclose(out_file);
        }
        printf("\n");
        parse(line, argv);
        if (strcmp(argv[0], "exit") == 0)
            exit(0);            
        execute(argv);           
     } 
}
